<?php

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;

require_once __DIR__ . '/LoginDataProvider.php';

class OrderSlipTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $module;
    protected $webDriver;

    protected $url = '/UserjyuchuIkkatu';

    protected $timeWaitAlert = 20;

    protected function _before()
    {
        $this->module = $this->getModule('WebDriver');
        LoginDataProvider::login($this->module, $this->url);
    }

    protected function _after()
    {
        // $this->module->_closeSession();
    }

    /**
     * @dataProvider getItemDataProvider
     */
    public function _testItemUpdate($options)
    {
        // print_r($options);

        $this->module->selectOption("#input_edit_fields[]", $options);
        $this->module->seeCurrentUrlEquals($this->url . '/search');
        $this->module->click('#jyuchu_dlg_open');
        $this->module->click('#ne_dlg_btn2_searchJyuchuIkkatuDlg');

        $blocks = $this->module->_findElements('#ikkaktu_form div.block-body');
        $this->_replaceItem($blocks, $inputs);
        $this->_addItem($blocks, $inputs);
        $this->_updateItem();

    }

    public function _replaceItem(array $blocks, array $inputs)
    {
        foreach ($blocks as $index => $block) {
            if ($inputs[$index]['type'] == 'datetime') {
                $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value']);
                $this->module->click('#ui-datepicker-div a.ui-state-active');
                $block->findElement(WebDriverBy::tagName('button'))->sendKeys(WebDriverKeys::ENTER);
            } else {
                if ($inputs[$index]['type'] == 'text') {
                    $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value']);
                } elseif ($inputs[$index]['type'] == 'textarea') {
                    $block->findElement(WebDriverBy::tagName('textarea'))->clear()->sendKeys($inputs[$index]['value']);
                } elseif ($inputs[$index]['type'] == 'checkbox') {
                    $block->findElement(WebDriverBy::tagName('input'))->click();
                } elseif ($inputs[$index]['type'] == 'select') {
                    $selectTag = $block->findElement(WebDriverBy::tagName('select'));
                    $select    = new WebDriverSelect($selectTag);
                    $select->selectByValue($inputs[$index]['value']);
                }

                $block->findElement(WebDriverBy::tagName('button'))->click();
            }

        }
    }

    public function _addItem(array $blocks, array $inputs)
    {
        $this->module->click('#ikkaktu_form>div.form-actions>a.btn');
        foreach ($blocks as $index => $block) {
            if ($inputs[$index]['action_add'] === true) {
                if ($inputs[$index]['type'] == 'text') {
                    $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value_add']);
                } elseif ($inputs[$index]['type'] == 'textarea') {
                    $block->findElement(WebDriverBy::tagName('textarea'))->clear()->sendKeys($inputs[$index]['value_add']);
                }

                $button = $block->findElements(WebDriverBy::tagName('button'));
                $button[1]->click();
            }
        }
    }

    public function _updateItem()
    {
        $this->module->click('#update_form div.form-actions button');
        $this->module->acceptPopup();
        $this->module->webDriver->wait(10)->until(
            WebDriverExpectedCondition::alertIsPresent()
        );
    }

    public function getItemDataProvider()
    {
        return array(
            array(
                array('受注日', '受注番号', 'ピッキング指示内容', '納品書印刷指示日', '出荷予定日', '重要チェック', '受注分類タグ', '作業用欄'),
                array(
                    array('type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('type' => 'text', 'value' => '10', 'action_add' => true, 'value_add' => '_追加'),
                    array('type' => 'textarea', 'value' => 'ﾋﾟｯｷﾝｸﾞ指示内容_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('type' => 'checkbox', 'action_add' => false),
                    array('type' => 'text', 'value' => '受注分類タグ_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('type' => 'textarea', 'value' => '作業用欄_更新', 'action_add' => true, 'value_add' => '_追加'),
                ),
            ),
            array(
                array('納品書特記事項', '受注キャンセル', '配達希望日', '発送伝票備考欄', '発送伝票番号', '備考', '承認状況', '顧客区分', '発送方法'),
                array(
                    array('value' => '納品書特記事項_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('value' => '0', 'action_add' => false),
                    array('value' => '2017/01/02', 'action_add' => false),
                    array('value' => '発送伝票備考欄_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('value' => '10', 'action_add' => true, 'value_add' => '_追加'),
                    array('value' => '備考_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('value' => '0', 'action_add' => false),
                    array('value' => '0', 'action_add' => false),
                    array('value' => '10', 'action_add' => false),
                ),
            ),
        );
    }
}
