<?php

require_once __DIR__ . '/LoginDataProvider.php';

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;

class UserBasicTest extends \Codeception\Test\Unit
{
    protected $module;

    protected $url = '/User_basic_userset';

    protected $timeWaitAlert = 30;

    protected function _before()
    {
        $this->module = $this->getModule('WebDriver');
        LoginDataProvider::login($this->module, $this->url);
    }

    protected function _after()
    {
        // $this->module->_closeSession();
    }

    /**
     * @dataProvider getAddUserDataProvider
     */
    public function testAddUser(array $params = array())
    {
        $countUser = 0;

        $tagEdits = $this->module->_findElements('#main a[name=edit]');
        if (!empty($tagEdits) && count($tagEdits) > 0) {
            $countUser = count($tagEdits);
        }

        $this->module->click("button[name=add]");


        $this->_fillForm("form[name=mainform]", $params);
        $this->module->click('form[name=mainform] button.btn');

        $this->_getMessageAlert('追加されました');

        $this->module->seeCurrentUrlEquals($this->url);

        $tagEdits = $this->module->_findElements("#main a[name=edit]");
        $this->assertEquals(
            $countUser + 1,
            count($tagEdits)
        );

    }

    /**
     * @dataProvider getEditUserDataProvider
     */
    public function testEditUser($index = 1, array $params = array())
    {

        $tagEdits = $this->module->_findElements("#main a[name=edit]");
        if (!empty($tagEdits) && count($tagEdits) > 0) {
            $tagEdits[$index]->click();

            $this->_fillForm("form[name=mainform]", $params);

            $this->module->click('form[name=mainform] button.btn');

            $this->_getMessageAlert('更新されました。');

            $this->module->seeCurrentUrlEquals($this->url);
        }

    }

    /**
     * @dataProvider getEditPasswordUserDataProvider
     */
    public function testEditPasswordUser($index = 1, array $params = array())
    {

        $tagEdits = $this->module->_findElements("#main a[name=edit]");
        if (!empty($tagEdits) && count($tagEdits) > 0) {
            $tagEdits[$index]->click();

            $this->module->click('#show-dialog-btn');

            $this->_fillForm("form[name=mainform]", $params);

            $this->module->click("#ne_dlg_btn1_dlg");

            // Update user
            $this->module->click('form[name=mainform] button.btn');

            $this->_getMessageAlert('更新されました。');
            $this->module->seeCurrentUrlEquals($this->url);
        }

    }

    /**
     * @dataProvider getDeleteUserDataProvider
     */
    public function testDeleteUser($index = 0)
    {

        $tagDeletes = $this->module->_findElements("#main a.btn-danger");
        if (!empty($tagDeletes) && count($tagDeletes) > 0) {
            $tagDeletes[$index]->click();
            $this->module->webDriver->wait($this->timeWaitAlert)->until(
                WebDriverExpectedCondition::alertIsPresent()
            );
            $this->module->acceptPopup();

            $this->_getMessageAlert('削除されました');

            $this->module->seeCurrentUrlEquals($this->url);
        } else {
            echo "Delete user: Don't have user to delete!\n";
        }

    }

    public function _getMessageAlert($message)
    {
        $this->module->webDriver->wait($this->timeWaitAlert)->until(
            WebDriverExpectedCondition::alertIsPresent()
        );
        $this->module->seeInPopup($message);
        $this->module->acceptPopup();

    }

    protected function getSubmissionFormFieldName($name)
    {
        if (substr($name, -2) === '[]') {
            return substr($name, 0, -2);
        }
        return $name;
    }

    public function _fillForm($selector, array $params)
    {
        $form = $this->module->webDriver->findElement(WebDriverBy::cssSelector($selector));

        $fields = $form->findElements(
            WebDriverBy::cssSelector('input:enabled,textarea:enabled,select:enabled,input[type=hidden]')
        );
        foreach ($fields as $field) {
            $fieldName = $this->getSubmissionFormFieldName($field->getAttribute('name'));
            if (!isset($params[$fieldName])) {
                continue;
            }
            $value = $params[$fieldName];
            if (is_array($value) && $field->getTagName() !== 'select') {
                if ($field->getAttribute('type') === 'checkbox' || $field->getAttribute('type') === 'radio') {
                    $found = false;
                    foreach ($value as $index => $val) {
                        if (!is_bool($val) && $val === $field->getAttribute('value')) {
                            array_splice($params[$fieldName], $index, 1);
                            $value = $val;
                            $found = true;
                            break;
                        }
                    }
                    if (!$found && !empty($value) && is_bool(reset($value))) {
                        $value = array_pop($params[$fieldName]);
                    }
                } else {
                    $value = array_pop($params[$fieldName]);
                }
            }

            if ($field->getAttribute('type') === 'checkbox' || $field->getAttribute('type') === 'radio') {
                if ($value === true || $value === $field->getAttribute('value')) {
                    $this->checkOption($field);
                } else {
                    $this->uncheckOption($field);
                }
            } elseif ($field->getAttribute('type') === 'button' || $field->getAttribute('type') === 'submit') {
                continue;
            } elseif ($field->getTagName() === 'select') {
                $this->selectOption($field, $value);
            } else {
                $form->findElement(WebDriverBy::name($fieldName))->clear()->sendKeys($value);
            }
        }
    }

    public function selectOption($el, $option)
    {
        $wdSelect = new WebDriverSelect($el);
        if ($wdSelect->isMultiple()) {
            $wdSelect->deselectAll();
        }
        if (!is_array($option)) {
            $option = [$option];
        }

        foreach ($option as $opt) {
            $wdSelect->selectByValue($opt);
        }

    }

    public function checkOption($field)
    {
        if ($field->isSelected()) {
            return;
        }
        $field->click();
    }

    public function uncheckOption($field)
    {
        if (!$field->isSelected()) {
            return;
        }
        $field->click();
    }

    public function getAddUserDataProvider()
    {
        $id = time();
        return array(
            array(
                array(
                    'user_code' => 'staff_user_' . $id,
                    'password' => 'ADD1234',
                    'password2' => 'ADD1234',
                    'tantou_name' => '山田太郎',
                    'mail_adr' => 'test_add_mail@hamee.co.jp',
                    'tantou_kana' => 'てすとぶしょ',
                    'busyo_name' => 'テスト部署',
                    'syakai_hoken_kbn' => '0',
                    'seinengapi' => '12/12/2000',
                    'yubin_bangou' => '123-456',
                    'jyusyo1' => '神奈川県小田原市栄町2-9-39',
                    'jyusyo2' => '小田原EPO 5F',
                    'keitai_bangou' => '090-1234-567',
                    'denwa' => '0120-12-3456',
                    'nyusya_bi' => '2/2/2015',
                    'taisyoku_bi' => '3/3/2020',
                    'tantou_tenpo[]' => ['2'],
                ),
            ),
        );
    }

    public function getEditUserDataProvider()
    {
        $id = time();
        return array(
            array(
                1,
                array(

                    'user_code' => 'staff_user_edit_' . $id,
                    'tantou_name' => '山田太郎_edit',
                    'mail_adr' => 'test_edit_mail@hamee.co.jp',
                    'tantou_kana' => 'てすとぶしょ',
                    'busyo_name' => 'テスト部署',
                    'syakai_hoken_kbn' => '0',
                    'seinengapi' => '12/12/2000',
                    'yubin_bangou' => '123-456',
                    'jyusyo1' => '神奈川県小田原市栄町2-9-39',
                    'jyusyo2' => '小田原EPO 5F',
                    'keitai_bangou' => '090-1234-567',
                    'denwa' => '0120-12-3456',
                    'nyusya_bi' => '2/2/2015',
                    'taisyoku_bi' => '3/3/2020',
                    'tantou_tenpo[]' => ['2'],
                ),
            ),
        );
    }

    public function getEditPasswordUserDataProvider()
    {
        return array(
            array(
                1,
                array(
                    'password' => 'EDIT1234',
                    'password2' => 'EDIT1234',
                ),
            ),
        );
    }

    public function getDeleteUserDataProvider()
    {
        return array(
            array(
                1,
            ),
        );
    }
}
