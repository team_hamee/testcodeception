<?php

require_once __DIR__ . '/LoginDataProvider.php';

class UserDownloadTest extends \Codeception\Test\Unit
{
    protected $module;

    protected $url = '/Userdownload/ndl';

    protected $timeWaitAlert = 20;

    protected function _before()
    {
        $this->module = $this->getModule('WebDriver');
        LoginDataProvider::login($this->module, $this->url);
    }

    protected function _after()
    {
        $this->module->_closeSession();
    }

    // tests
    public function testSomeFeature()
    {

    }
}
